class BattleController < ApplicationController
  def index
    @Q_num  = Question.count
    @battle = Question.find(@Q_num)
    @answer = @battle.answer
    # @battle = Qustion.find(rand(@battlelist.length - 1))
    respond_to do |format|
    format.pdf {
      input_filename = Rails.root.join(@battle.ques_url)

      page_count = PDF::Reader.new(input_filename).page_count

      file = Prawn::Document.new(:skip_page_creation => true) do |pdf|

        page_count.times do |num|
        pdf.start_new_page(:template => input_filename, :template_page => num+1)
        pdf.move_down(700)
        pdf.text('FOOTER TEXT')
        end

      end
      send_data file.render
     }
     end
  end


